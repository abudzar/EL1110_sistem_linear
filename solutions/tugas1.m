clear; close all; clc;

% soal
y0 = [-2 -2 -1 -1  0  0  1  1  2;
       0 -1  0  1  1  2  2  1  0];
subplot(331);
plot(y0(1,:),y0(2,:));
grid on;
xlim([min(y0(1,:))-1,max(y0(1,:))+1]);
ylim([min(y0(2,:))-1,max(y0(2,:))+1]);
title('x(t)');


% jawaban
% no.1
y1(1,:) = y0(1,:) + 1;
y1(2,:) = y0(2,:);
subplot(332);
plot(y1(1,:),y1(2,:));
grid on;
xlim([min(y1(1,:))-1,max(y1(1,:))+1]);
ylim([min(y1(2,:))-1,max(y1(2,:))+1]);
title('x(t-1)');

% no.2
y2(1,:) = 2 - y0(1,:);
y2(2,:) = y0(2,:);
subplot(333);
plot(y2(1,:),y2(2,:));
grid on;
xlim([min(y2(1,:))-1,max(y2(1,:))+1]);
ylim([min(y2(2,:))-1,max(y2(2,:))+1]);
title('x(2-t)');

% no.3
y3(1,:) = (y0(1,:) - 1) / 2;
y3(2,:) = y0(2,:);
subplot(334);
plot(y3(1,:),y3(2,:));
grid on;
xlim([min(y3(1,:))-1,max(y3(1,:))+1]);
ylim([min(y3(2,:))-1,max(y3(2,:))+1]);
title('x(2t+1)');

% no.4
y4(1,:) = 8 - 2 * y0(1,:);
y4(2,:) = y0(2,:);
subplot(335);
plot(y4(1,:),y4(2,:));
grid on;
xlim([min(y4(1,:))-1,max(y4(1,:))+1]);
ylim([min(y4(2,:))-1,max(y4(2,:))+1]);
title('x(4-t/2)');

% no.5
t5a = -t0;
mirLoc = min(find(t0 == 0));
y5 = y0 + flip(y0,mirLoc);
y5a = ;
t5a = 0:1:length(t0) - 1;
y5 = ones(size(t5a));
t5b = 3 - t5a;
len = 0;

for idx = 1:length(t5b)
    len = len + length(find(t5b(idx) == t0));
end

loc = zeros(1,len);

counter = 1;
for idx = 1:length(t5b)
    if length(find(t5b(idx) == t0)) == 1
        loc(counter) = find(t5b(idx) == t0);
        counter += 1;
    elseif length(find(t5b(idx) == t0)) == 2
        val = find(t5b(idx) == t0);
        loc(counter) = val(1);
        loc(counter+1) = val(2);
        counter += 2;
    end
end


t5 = t0(loc);   
subplot(326);
plot(t6,min(max(y0(loc),1),1));
grid on;
xlim([min(t5)-1,max(t6)+1]);
ylim([min(y0)-1,max(y0)+1]);
title('x(t)');




% subplot(321);
% plot(t0,y0);
% grid on;
% xlim([min(t0)-1,max(t0)+1]);
% ylim([min(y0)-1,max(y0)+1]);
% title('x(t)');
