x = [-4 1 -7 1 -0 3 -7] % ganjil
% x = [4 -1 7 -1 0 -2 6] % genap
h = [0 1 7 3 7]
y = conv(x,h)
fig=0;
if fig == 1
    figure(1);
    subplot(311);
    stem(x); grid on;
    subplot(312);
    stem(h); grid on;
    subplot(313);
    stem(y); grid on;
end