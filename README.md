# Resources EL1110 - Sistem Linear
## Teknik Elektro - Institut Teknologi Kalimantan

### KONTEN: 

- codes         : berisi code Matlab/Octave
- docs          : berisi file doc/docx
- notes         : berisi scan catatan kuliah
- picts         : berisi file-file gambar (.eps, .fig, .svg, etc)
- practices     : berisi modul praktikum
- references    : berisi buku-buku referensi yang digunakan selama kuliah
- slides        : berisi file presentasi kuliah
- solutions     : berisi solusi-solusi dari tugas-tugas tahun sebelumnya
- wavs          : berisi contoh sinyal yang digunakan

### UPLOAD PRAKTIKUM/TUGAS
- will be posted later

### BACAAN
- [Wikibooks tentang Zero-input, Zero-state, Impulse Response, dan Step response](https://en.wikibooks.org/wiki/Signals_and_Systems/Time_Domain_Analysis/System_Response)
- [FFT, Convolution, Correlation and Image Processing](http://indico.ictp.it/event/a14296/session/167/contribution/803/material/video/)
- [MOOC: Digital Signal Proessing](https://www.coursera.org/course/dsp)
- [MOOC: Audio Signal Processing](https://www.coursera.org/course/dsp)
- [Signals and Systems](https://www.princeton.edu/~cuff/ele301/schedule.html)
